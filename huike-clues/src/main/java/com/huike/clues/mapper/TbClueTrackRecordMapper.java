package com.huike.clues.mapper;

import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 线索跟进记录Mapper接口
 */
public interface TbClueTrackRecordMapper {


    /**
     * 新增线索跟进记录
     *
     */
    public int insertTbClueTrackRecord(TbClueTrackRecord tbClueTrackRecord);


    /*
     * 查询线索跟进记录列表
     * */
    List<TbClueTrackRecord> selectTrackRecordList(Long clueId);
}
