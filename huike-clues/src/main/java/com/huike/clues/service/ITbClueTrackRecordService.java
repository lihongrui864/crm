package com.huike.clues.service;

import com.huike.clues.domain.TbClueTrackRecord;

import java.util.List;

public interface ITbClueTrackRecordService {


    /*
     * 新增线索跟进记录
     * */
    public int addTrackRecord(TbClueTrackRecord tbClueTrackRecord);


    /*
    * 查询线索跟进记录列表
    * */
    List<TbClueTrackRecord> selectTrackRecordList(Long clueId);
}
