package com.huike.clues.service.impl;

import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.mapper.TbClueTrackRecordMapper;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.utils.DateUtils;
import com.huike.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ITbClueTrackRecordServiceImpl implements ITbClueTrackRecordService {

    @Autowired
    private TbClueTrackRecordMapper clueTrackRecordMapper;

    /*
     * 新增线索跟进记录
     * */
    @Override
    public int addTrackRecord(TbClueTrackRecord tbClueTrackRecord) {
        //设置跟进记录创建者和创建时间
        tbClueTrackRecord.setCreateTime(DateUtils.getNowDate());
        tbClueTrackRecord.setCreateBy(SecurityUtils.getUsername());
        tbClueTrackRecord.setType("0"); //修改跟进类型为：正常跟进
        return clueTrackRecordMapper.insertTbClueTrackRecord(tbClueTrackRecord);

    }

    /*
     * 查询线索跟进记录列表
     * */
    @Override
    public List<TbClueTrackRecord> selectTrackRecordList(Long clueId) {
        List<TbClueTrackRecord> recordList = clueTrackRecordMapper.selectTrackRecordList(clueId);
        return recordList;
    }
}
