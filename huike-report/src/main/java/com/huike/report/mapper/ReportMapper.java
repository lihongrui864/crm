package com.huike.report.mapper;

import com.huike.report.domain.vo.BusinessBillboardVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 首页统计分析的Mapper
 * @author Administrator
 *
 */
public interface ReportMapper {
	/**=========================================基本数据========================================*/
	/**
	 * 获取线索数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
    Integer getCluesNum(@Param("startTime") String beginCreateTime,
						@Param("endTime") String endCreateTime,
						@Param("username") String username);

	/**
	 * 获取商机数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getBusinessNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同数量
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Integer getContractNum(@Param("startTime") String beginCreateTime,
						   @Param("endTime") String endCreateTime,
						   @Param("username") String username);

	/**
	 * 获取合同金额
	 * @param beginCreateTime	开始时间
	 * @param endCreateTime		结束时间
	 * @param username			用户名
	 * @return
	 */
	Double getSalesAmount(@Param("startTime") String beginCreateTime,
						  @Param("endTime") String endCreateTime,
						  @Param("username") String username);

	/**=========================================今日简报========================================*/
	/**
	 * 获取今日新增线索数
	 * @param today		时间
	 * @param username	用户名
	 * @return
	 */
	Integer getTodayCluesNum(@Param("today") String today,@Param("username") String username);

	/**
	 * 获取今日新增商机数
	 * @param today		时间
	 * @param username	用户名
	 * @return
	 */
	Integer getTodayBusinessNum(@Param("today") String today,@Param("username") String username);

	/**
	 * 获取今日新增合同信息
	 * @param today  时间
	 * @param username 用户名
	 * @return
	 */
	Map<String, Object> getReportMsg(@Param("today") String today,@Param("username") String username);

	/**=========================================待办========================================*/
	Integer getToallocatedNumByType(@Param("startTime") String startTime, @Param("endTime") String endTime, @Param("type") String type);

	Integer getTofollowedNumByType(@Param("startTime") String startTime,@Param("endTime")  String endTime, @Param("type") String type);

	/**=========================================统计分析========================================*/
//    List<Map<String, Object>> queryClueReportByDateRange(@Param("startTime") String startTime,@Param("endTime")  String endTime);
	List<Map<String, Object>> queryClueReportByDateRange(@Param("startTime") String startTime,@Param("endTime")  String endTime);

    List<Map<String, Object>> subjectStatistics(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	Integer getCluesNums(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	Integer getEffectiveCluesNums(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	Integer getBusinessNums(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	Integer getContractNums(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	Integer queryBusinessTotal(@Param("startTime") String startTime,@Param("endTime")  String endTime);

	List<BusinessBillboardVo> businessChangeStatistics(@Param("startTime") String startTime,@Param("endTime")  String endTime,@Param("businessTotal") Integer businessTotal);

	List<BusinessBillboardVo> salesStatistic(@Param("startTime") String startTime,@Param("endTime")  String endTime,@Param("clueTotal") Integer clueTotal);
}
