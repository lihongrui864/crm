package com.huike.report.service;

import com.huike.clues.domain.TbActivity;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.vo.IndexStatisticsVo;
import com.huike.contract.domain.TbContract;
import com.huike.report.domain.vo.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface IReportService {


    /**
     * 客户统计报表
     * @param tbContract
     * @return
     */
    public  List<TbContract> contractReportList(TbContract tbContract);



    /**
     * 销售统计部门报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> deptStatisticsList(String beginCreateTime, String endCreateTime);

    /**
     * 销售统计渠道报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> channelStatisticsList(String beginCreateTime, String endCreateTime);
    /**
     * 销售统计归属人报表
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> ownerShipStatisticsList(String beginCreateTime, String endCreateTime);



    /**
     * 渠道统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> chanelStatistics(String beginCreateTime, String endCreateTime);


    /**
     * 活动统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    public List<Map<String, Object>> activityStatistics(String beginCreateTime, String endCreateTime);



    public List<TbClue> cluesStatisticsList(TbClue clue);

    /**
     * 活动渠道统计
     * @param activity
     * @return
     */
    public List<ActivityStatisticsVo> activityStatisticsList(TbActivity activity);



    public IndexVo getIndex(IndexStatisticsVo request);


    /**
     * 首页基本数据展示
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    IndexBaseInfoVO getBaseInfo(String beginCreateTime, String endCreateTime);

    /**
     * 首页今日简报数据展示
     * @return
     */
    IndexTodayInfoVO getTodayInfo();

    /**
     * 首页待办数据统计
     * @return
     */
    IndexTodoInfoVO getTodoInfo(String beginCreateTime, String endCreateTime);

    /**
     * 统计分析-线索统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    Map<String, List> cluesStatistics(String beginCreateTime, String endCreateTime) throws ParseException;

    /*统计分析-新增客户（合同）饼状统计图*/
    List<Map<String,Object>> subjectStatistics(String beginCreateTime, String endCreateTime);

    /*统计分析 - 线索统计 - 线索转化率统计 */
    VulnerabilityVO getVulnerabilityMap(String beginCreateTime, String endCreateTime);

    List<BusinessBillboardVo> businessChangeStatistics(String beginCreateTime, String endCreateTime);

    List<BusinessBillboardVo> salesStatistic(String beginCreateTime, String endCreateTime);
}
