package com.huike.report.domain.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BusinessBillboardVo implements Serializable {
    private String create_by;  //用户名称
    private String deptName;  //部门名称
    private Integer num = 0;  //转化数量
    private Double radio = 0.00;  //转化c率
}
