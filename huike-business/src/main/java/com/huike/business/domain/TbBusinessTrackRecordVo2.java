package com.huike.business.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
@Data
public class TbBusinessTrackRecordVo2 {
    private String businessId;
    private String createBy;
    private String keyItems;
    private String record;
    private String createTime;
    private String trackStatus;
    private String nextTime;        //下次跟进时间
}
