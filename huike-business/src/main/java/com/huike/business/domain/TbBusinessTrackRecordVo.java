package com.huike.business.domain;

import lombok.Data;

@Data
public class TbBusinessTrackRecordVo{
    private int id;
    private String name;
    private String phone;
    private String occupation;
    private String education;
    private String provinces;
    private String city;
    private String weixin;
    private int age;
    private String major;
    private String job;
    private String salary;
    private String qq;
    private String sex;
    private String expectedSalary;
    private String remark;
    private String subject;
    private String reasons;
    private String plan;
    private String planTime;
    private String courseId;
    private String otherIntention;
    private String trackStatus;     //跟进状态
    private String nextTime;        //下次跟进时间
    private String keyItems;        //沟通重点
    private String record;      //沟通纪要
    private String createBy;    //创建人
    private String createTime;  //创建时间
    private String channel;
    private String activityId;
    private String businessId;

}
