package com.huike.web.controller.clues;

import com.huike.business.domain.TbBusiness;
import com.huike.business.domain.TbBusinessTrackRecordVo;
import com.huike.business.domain.TbBusinessTrackRecordVo2;
import com.huike.business.service.ITbBusinessService;
import com.huike.clues.domain.TbClue;
import com.huike.clues.domain.TbClueTrackRecord;
import com.huike.clues.domain.dto.FalseClueDto;
import com.huike.clues.domain.vo.ClueTrackRecordVo;
import com.huike.clues.service.ITbClueService;
import com.huike.common.core.domain.AjaxResult;
import org.springframework.beans.BeanUtils;
import com.huike.clues.service.ITbClueTrackRecordService;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.page.TableDataInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
public class TestClueBusinessController extends BaseController {
    @Autowired
    private ITbClueService tbClueService;

    @Autowired
    private ITbBusinessService tbBusinessService;

    @Autowired
    private ITbClueTrackRecordService clueTrackRecordService;

    /*
     * 新增线索跟进记录
     *
     */
    @PostMapping("/clues/record")
    public AjaxResult addTrackRecord(@RequestBody ClueTrackRecordVo clueTrackRecordVo) {
        //将VO对象拆分开 分别进行 线索更新 和 跟进添加 操作
        TbClueTrackRecord tbClueTrackRecord = new TbClueTrackRecord();
        TbClue tbClue = new TbClue();
        BeanUtils.copyProperties(clueTrackRecordVo, tbClueTrackRecord);
        BeanUtils.copyProperties(clueTrackRecordVo, tbClue);

        //将线索id 主外键进行转换设置  设置状态为 跟进中
        tbClue.setId(clueTrackRecordVo.getClueId());
        tbClue.setStatus(TbClue.StatusType.FOLLOWING.getValue());

        //调用方法 更新线索记录
        tbClueService.updateTbClue(tbClue);
        //调用方法 添加跟进记录
        clueTrackRecordService.addTrackRecord(tbClueTrackRecord);
        return AjaxResult.success();
    }

    //查询线索跟进记录集合
    @GetMapping("/clues/record/list")
    public TableDataInfo getRecordList(@RequestParam("clueId") Long clueId) {
        startPage();
        List<TbClueTrackRecord> recordList = clueTrackRecordService.selectTrackRecordList(clueId);
        return getDataTable(recordList);
    }

    //上报伪线索
    @PutMapping("/clues/clue/false/{id}")
    public AjaxResult falseClue (@PathVariable Long id,@RequestBody FalseClueDto dto){
        return toAjax(tbClueService.falseClue(id, dto.getReason(), dto.getRemark()));
    }

    /*
    *新增商机跟进记录
    */
    @PostMapping("/business/record")
    public AjaxResult addRecord(@RequestBody TbBusinessTrackRecordVo tbBusinessTrackRecordVo) throws ParseException {
        System.out.println(tbBusinessTrackRecordVo.toString());
        System.out.println("新增商机记录");
        System.out.println("------------------");

        //将客户资料和客户意向的信息更新一下
        //将客户资料和客户意向的信息更新一下
        TbBusiness tbBusiness = new TbBusiness();
        //将vo的数据先赋值给tbBusiness对象
        BeanUtils.copyProperties(tbBusinessTrackRecordVo, tbBusiness);
        String businessId = tbBusinessTrackRecordVo.getBusinessId();
        int bussinessId = Integer.parseInt(businessId);
        tbBusiness.setBusinessId(bussinessId);
        TbBusinessTrackRecordVo2 vo2 = new TbBusinessTrackRecordVo2();
        String nextTime = tbBusinessTrackRecordVo.getNextTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date parse = dateFormat.parse(nextTime);
        tbBusiness.setNextTime(parse);
        //执行修改操作
        tbBusinessService.updateTbBusiness(tbBusiness);
        //添加商机跟进记录
        BeanUtils.copyProperties(tbBusinessTrackRecordVo, vo2);
        vo2.setBusinessId(tbBusinessTrackRecordVo.getBusinessId());
        System.out.println(vo2.toString());
        //添加商机跟进记录操作
        tbBusinessService.insertAddTrackRecord(vo2);
        AjaxResult ajaxResult = new AjaxResult(200, "操作成功！");
        return ajaxResult;
    }

    /*
    *查询商机跟进记录列表
    *
    */
    @GetMapping("business/record/list")
    public AjaxResult RecordList(@RequestParam("businessId") String businessId) {
        String bus = businessId;
        System.out.println(businessId);
        //根据id查询该客户的跟进记录
        List<TbBusinessTrackRecordVo2> vo2s = tbBusinessService.listRecord(bus);
        System.out.println(vo2s);
        AjaxResult ajaxResult = new AjaxResult(200, "操作成功", vo2s);
        return ajaxResult;
    }


}
