package com.huike.web.controller.common;

import com.huike.common.config.HuiKeConfig;
import com.huike.common.constant.Constants;
import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.utils.StringUtils;
import com.huike.common.utils.file.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 通用请求处理
 * 
 * 
 */
@RestController
public class CommonController extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(CommonController.class);

	/**
	 * 通用下载请求
	 * 
	 * @param fileName 文件名称
	 */
	@GetMapping("common/download")
	public void fileDownload(String fileName, HttpServletResponse response, HttpServletRequest request) {
		try {
			String filePath = HuiKeConfig.getDownloadPath() + fileName;
			if (!FileUtils.checkAllowDownload(filePath)) {
				throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", fileName));
			}
			// 数据库资源地址
			System.out.println("downloadPath---" + filePath);
			// 下载名称
			String downloadName = StringUtils.substringAfterLast(filePath, "/");
			response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
			FileUtils.setAttachmentResponseHeader(response, downloadName);
			FileUtils.writeBytes(filePath, response.getOutputStream());
		} catch (Exception e) {
			log.error("下载文件失败", e);
		}

	}

	/**
	 * 通用上传请求
	 */
	@PostMapping("/common/upload")
	public AjaxResult uploadFile(MultipartFile file) throws Exception {
		return null;
	}

	/**
	 * 本地资源通用下载
	 */
	@GetMapping("/download/resource")
	public void resourceDownload(String resource, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try {
			if (!FileUtils.checkAllowDownload(resource)) {
				throw new Exception(StringUtils.format("资源文件({})非法，不允许下载。 ", resource));
			}
			// 本地资源路径
			String localPath = HuiKeConfig.getProfile();
			// 数据库资源地址
			String downloadPath = localPath + StringUtils.substringAfter(resource, Constants.RESOURCE_PREFIX);
			System.out.println("downloadPath---" + downloadPath);
			// 下载名称
			String downloadName = StringUtils.substringAfterLast(downloadPath, "/");
			response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
			FileUtils.setAttachmentResponseHeader(response, downloadName);
			FileUtils.writeBytes(downloadPath, response.getOutputStream());
		} catch (Exception e) {
			log.error("下载文件失败", e);
		}
	}
}
