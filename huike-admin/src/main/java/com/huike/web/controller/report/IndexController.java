package com.huike.web.controller.report;


import com.huike.common.core.domain.AjaxResult;
import com.huike.report.service.IReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/index")
public class IndexController {
    @Autowired
    private IReportService reportService;

    /**
     * 首页--基础数据统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getBaseInfo")
    public AjaxResult getBaseInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.getBaseInfo(beginCreateTime,endCreateTime));
    }

    /**
     * 首页--今日简报
     * @return
     */
    @GetMapping("/getTodayInfo")
    public AjaxResult getTodayInfo(){
        return AjaxResult.success(reportService.getTodayInfo());
    }


    /**
     * 首页---待办数据统计
     * @return
     */
    @GetMapping("/getTodoInfo")
    public AjaxResult getTodoInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.getTodoInfo(beginCreateTime,endCreateTime));
    }

    /**
     * 首页---商机转化龙虎榜
     * @return
     */
    @GetMapping("/businessChangeStatistics")
    public AjaxResult businessChangeStatistics(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.businessChangeStatistics(beginCreateTime, endCreateTime));
    }


    /**
     * 首页---线索转化龙虎榜
     * @return
     */
    @GetMapping("/salesStatistic")
    public AjaxResult salesStatistic(@RequestParam("beginCreateTime") String beginCreateTime,
                                               @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.salesStatistic(beginCreateTime, endCreateTime));
    }
}
